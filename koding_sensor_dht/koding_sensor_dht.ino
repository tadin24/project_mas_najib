#include <DHT.h>
#include <DHT_U.h>

// menginisialisasikan nama baru library DHT menjadi dht
DHT dht(2, DHT11); // inisial pin baca sensor DHT berada pada pin 2 arduino

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // memulai komunikasi serial dengan baudrate 9600
  dht.begin(); // memulai penggunaan library dht
}

void loop() {
  // put your main code here, to run repeatedly:
  float kelembaban = dht.readHumidity(); // membaca kelembaban
  float celcius = dht.readTemperature(); // membaca suhu

  // konversi suhu celcius ke reamur
  float reamur = celcius * 4 / 5;

  // konversi suhu celcius ke fahrenheit
  float fahrenheit = celcius * 9 / 5 + 32;

  // konversi suhu celcius ke kelvin
  float kelvin = celcius + 273;

  Serial.print("suhu Reamur : ");
  Serial.println(reamur);
  Serial.print("suhu Celcius : ");
  Serial.println(celcius);
  Serial.print("suhu Fahrenheit : ");
  Serial.println(fahrenheit);
  Serial.print("suhu Kelvin : ");
  Serial.println(kelvin);
  Serial.print("Kelembaban : ");
  Serial.println(kelembaban);
  delay(3000);
}
