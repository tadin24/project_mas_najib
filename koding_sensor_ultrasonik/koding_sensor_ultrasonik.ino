#define trigger 4 // mendefinisikan trigger pada pin 4
#define echo 2 // mendefinisikan echo pada pin 2

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // memulai Serial
  pinMode(trigger, OUTPUT); // merubah mode pin trigger menjadi OUTPUT
  pinMode(echo, INPUT); // merubah mode pin echo menjadi INPUT
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(trigger, HIGH); // mengirim suara
  delayMicroseconds(10); // selama 10 mikro detik
  digitalWrite(trigger, LOW); // berhenti mengirim suara

  float dataMasuk = pulseIn(echo, HIGH); // membaca data dan dimasukkan ke variabel dataMasuk
  float waktu = dataMasuk / 1000000; // konversi dari mikro detik ke detik
  float jarakMeter = waktu * 340 / 2; // menghitung jarak dengan menggunakan waktu dalam satuan meter
  float jarakCentiMeter = jarakMeter * 100; // konversi dari meter ke centimeter

  Serial.print("jarak : ");
  Serial.println(jarakCentiMeter);

  delay(500); // delay 500ms
}
